import UIKit

func selectionSort<T: Comparable>(array: inout[T],tang: Bool) {
    for i in 0..<array.count - 1 {                              //(1)
        var max = i                                             //(2)
        for j in i+1..<array.count {                            //(3)
            if (tang && array[j] < array[max]) || (!tang && array[j] > array[max]) {                          //(4)
                max = j                                         //(5)
            }
        }
        // swap array[i], array[min]
        let temp = array[i]                                     //(6)
        array[i] = array[max]                                   //(7)
        array[max] = temp                                       //(8)
    }
}

func demoSelectionSort<T: Comparable>(array: inout[T],tang: Bool) {
    for i in 0..<array.count - 1 {                              //(1)
        print("(1) i = \(i)")
        var max = i                                             //(2)
        print(" (2) max = \(max)")
        for j in i+1..<array.count {                            //(3)
            print(" (3) j = \(j)")
            if tang {
                print("  (4) array[\(j)] = \(array[j]) < array[\(max)] = \(array[max]) ? \(array[j] < array[max])")
            }else {
                print("  (4) array[\(j)] = \(array[j]) > array[\(max)] = \(array[max]) ? \(array[j] > array[max])")
            }
            if (tang && array[j] < array[max]) || (!tang && array[j] > array[max]) {                          //(4)
                max = j                                         //(5)
                print("    (5) max = \(max)")
            }
        }
        // swap array[i], array[min]
        let temp = array[i]                                     //(6)
        array[i] = array[max]                                   //(7)
        array[max] = temp                                       //(8)
        print(" (6),(7),(8) swap array[\(i)] = \(array[i]) và array[\(max)] = \(array[max])")
        print("Mãng đã sắp xếp lần thứ \(i) này là: \(array)")
        print(("-------------"))
    }
}

print("SELECTSORT")
print("Sắp xếp mảng [10,1,4,0,2,1,6] theo thứ tự giảm dần")
var array = [10,1,4,0,2,1,6]
demoSelectionSort(array: &array,tang: false)
print("Kết quả: \(array)")
