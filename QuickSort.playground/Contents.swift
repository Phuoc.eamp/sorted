import UIKit

func quicksort<T: Comparable>(_ array: [T], tang: Bool) -> [T] {
    guard array.count > 1 else { return array }     //(1)
    let soduocchon = array[array.startIndex]        //(2)
    var behon = [T]()
    var bang = [soduocchon]
    var lonhon = [T]()
    for i in 1..<array.count {                      //(3)
        if array[i] < soduocchon {                  //(4)
            behon.append(array[i])                  //(5)
        }else if array[i] > soduocchon {            //(6)
            lonhon.append(array[i])                 //(7)
        }else {                                     //(6)
            bang.append(array[i])                   //(8)
        }
    }
    let result = tang ? quicksort(behon,tang: tang) + bang + quicksort(lonhon,tang: tang) : quicksort(lonhon,tang: tang) + bang + quicksort(behon,tang: tang)
    return result
}

func demoQuicksort<T: Comparable>(_ array: [T],tang: Bool) -> [T] {
    print("Với array = \(array)")
    print(" (1) array.count > 1 ? \(array.count > 1)")
    guard array.count > 1 else {                //(1)
        print(" array = \(array)")
        print("---------------")
        return array
    }
    let soduocchon = array[array.startIndex]        //(2)
    print("     (2) số được chọn = \(soduocchon)")
    var behon = [T]()
    var bang = [soduocchon]
    var lonhon = [T]()
    for i in 1..<array.count {                      //(3)
        print("     (3) i = \(i)")
        if array[i] < soduocchon {                  //(4)
            print("         (4) array[\(i)] = \(array[i]) < \(soduocchon) ? \(array[i] < soduocchon)")
            behon.append(array[i])                  //(5)
            print("             (5) behon = \(behon)")
        }else if array[i] > soduocchon {            //(6)
            print("         (6) array[\(i)] = \(array[i]) > \(soduocchon) ? \(array[i] > soduocchon)")
            lonhon.append(array[i])                 //(7)
            print("             (5) lonhon = \(lonhon)")
        }else {                                     //(6)
            print("         (8) array[\(i)] = \(soduocchon) ? \(array[i] < soduocchon)")
            bang.append(array[i])                   //(8)
            print("             (5) bang = \(bang)")
        }
    }
    print(" behon: \(behon)")
    print(" lonhon: \(lonhon)")
    print(" bang: \(bang)")
    print("---------")
    
    let result = tang ? demoQuicksort(behon,tang: tang) + bang + demoQuicksort(lonhon,tang: tang) : demoQuicksort(lonhon,tang: tang) + bang + demoQuicksort(behon,tang: tang)
    print("Mảng được \(array) sắp xếp: \(result)")
    return result
}

print("QIUCK SORT")
print("Sắp xếp mảng [3,1,6,4,2,0,1,15] theo thứ tự tăng dần")
var arr  = [3,1,6,4,2,0,1,15]
demoQuicksort(arr,tang: true)

