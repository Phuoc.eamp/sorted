import UIKit

func insertionSort<T: Comparable>(array: inout[T],tang: Bool) {
    var max: T
    for i in 1..<array.count {                       //(1)
        max = array[i]                               //(2)
        var j = i - 1                                //(3)
        while j >= 0 && ((tang && array[j] > max) || (!tang && array[j] < max)) {             //(4)
            array[j+1] = array[j]                    //(5)
            j -= 1                                   //(6)
        }
        array[j+1] = max                             //(7)
    }
}

func demoInsertionSort<T: Comparable>(array: inout[T],tang: Bool) {
    var max: T
    for i in 1..<array.count {                       //(1)
        print("(1) i = \(i)")
        max = array[i]                               //(2)
        print(" (2) max = \(max)")
        var j = i - 1                                //(3)
        print(" (3) j = \(j)")
        print(" (4)  j = \(j) >= 0 && array[\(j)] = \(array[j]) > max = \(max) ? \(j >= 0 && array[j] > max)")
        while j >= 0 && ((tang && array[j] > max) || (!tang && array[j] < max)) {             //(4)
            array[j+1] = array[j]                    //(5)
            print("   (5) array[\(j+1)] = \(array[j+1])")
            j -= 1                                   //(6)
            print("   (6) j = \(j)")
            if j >= 0 {
                print(" (4)  j = \(j) >= 0 && array[\(j + 1)] = \(array[j]) > max = \(max) ? \(j >= 0 && array[j] > max)")
            }else {
                print(" (4)  j = \(j) >= 0 ? \(j >= 0)")
            }
        }
        array[j+1] = max                             //(7)
        print(" (7) array [\(j+1)] = \(array[j+1])")
        print("Mảng đã sắp xếp lần thứ \(i): \(array)")
        print("--------------")
    }
}

print("InsertionSort")
var arr = [10,1,4,0,2,1,6]
print("Sắp xếp mảng sau theo thứ tự tăng dần: \(arr)")
demoInsertionSort(array: &arr,tang: true)
print("Kết quả: \(arr)")


